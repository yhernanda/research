$(document).ready(function() {
    //$('#tabMentah').DataTable();
    //$('#tabNormalisasi').DataTable();            
       
    $('#rangestart').calendar({
        type: 'date',
        endCalendar: $('#rangeend'),
        monthFirst: false,
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return year + '/' + month + '/' + day;
          }
        }
    });
    $('#rangeend').calendar({
        type: 'date',
        startCalendar: $('#rangestart'),
        monthFirst: false,
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return year + '/' + month + '/' + day;
          }
        }
    });
});

$(document).on('click', 'a.sidemenu', function (e){
    $(".active").removeClass("active");
    var active = e.target;
    $(active).addClass("active");
});

$(document).on('change', '#selectNormalisasi', function(e){    
    $('#btnNormalisasi').attr('href', $(e.target).val());    
});

/*$(document).on('click', '#normalizeFact', function(event){
   $.ajax({
        type: "POST",
        url: "<s:url action='normalizeFact'/>",
        success: function(){
            //alert("did it!");
        }
   })
});*/