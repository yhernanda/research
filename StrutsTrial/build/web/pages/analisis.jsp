<%-- 
    Document   : analysis
    Created on : Sep 13, 2016, 11:51:52 AM
    Author     : yhernanda
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="simple.Fact"%>
<%@page import="simple.Pivot"%>
<%@page import="simple.Strip"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Analisis</title>
       
        <link rel="stylesheet" type="text/css" href="Source/css/table.css">
        <link rel="stylesheet" type="text/css" href="Source/css/semantic.min.css">
        <link rel="stylesheet" type="text/css" href="Source/css/dataTables.semanticui.min.css">
        <script src="Source/js/jquery-2.2.3.min.js"></script>
        <script src="Source/js/semantic.min.js"></script>
        <script src="Source/js/dataTables.semanticui.min.js"></script>
        <script src="Source/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="Source/js/pivotCluster.js"></script>
    </head>
    <body>       
        <!-- Welcome, you have logined. <br />
        The attribute of 'context' in session is
        <ww:property value="#session.context" />
        <br /><br /><br />
        <a xhref="<%= request.getContextPath() %>/logout.action">Logout</a>
        <br />
        <a xhref="<%= request.getContextPath() %>/logout2.action">Logout2</a> -->
        
        <div class="ui container">
            <br>
            <div class="ui secondary menu">
              <div class="header item">
                  <i class="big home icon"></i>
                  Nearest Neighbor Clustering
              </div>
              <div class="right menu">
                <div class="item">
                  <div class="ui icon input">
                    <input type="text" placeholder="Search...">
                    <i class="search link icon"></i>
                  </div>
                </div>
                <a class="ui item" href="action/logout">
                  <i class="large user icon"></i>
                  <s:property value="#session['USER']"/>
                </a>
              </div>
            </div>
            <div class="ui divider"></div>
            <br>
            <div class="ui grid">
              <div class="four wide column">
                <div class="ui vertical pointing menu">
                  <a href="<s:url action="inputDataPage"/>" id="dashboard" class="item sidemenu">
                    <i class="large database icon"></i>
                    Input Data
                  </a>
                  <a id="inputData" class="item active sidemenu">
                    <i class="large edit icon"></i>
                    Pivot Table & Distance
                  </a>
                  <a href="<s:url action="clusterPurity"/>" class="item sidemenu">
                    <i class="large dashboard icon"></i>
                    Purity & Scatter Plot
                  </a>
                </div>
              </div>
              
              <!-- DASHBOARD -->
              <div id="inputData-view" class="ui grid twelve wide column">                
                <form id="formClustering" action="euclideanDistance" method="post" class="ui form">                                  
                    <div class="three fields">
                        <div class="field">
                            <label>Metode</label>
                            <div class="ui">
                                <select id="selectClustering">
                                    <option value="euclideanDistance">Euclidean Distance</option>
                                    <option value="cosineSimilarity">Cosine Similarity</option>
                                </select>
                            </div>
                        </div>
                        <div class="field">
                            <label>Threshold</label>
                            <div class="ui">
                              <div class="ui input left icon">
                                <i class="number icon"></i>
                                <input type="number" step="0.0001" placeholder="Threshold" name="threshold" id="threshold">
                              </div>
                            </div>
                        </div>
                        <div class="field" style="display: flex; justify-content:bottom; align-items:center;">
                            <input type="submit" value="Calculate Distance & Clustering" class="ui fluid teal submit button">                                                
                        </div>
                    </div>                                                                                                
                </form>
                <h2>Tabel Hasil Pivot</h2>
                <div style="width:100%; overflow-y:scroll;">
                    <display:table export="true" name="pivotListJenis" id="tabHive" class="ui celled table" cellspacing="0" sort="list" pagesize="20" requestURI="">                  
                        <display:column property="tanggal" title="Tanggal" sortable="true" headerClass="sortable" />
                        <display:column property="no_nota" title="No. Nota" sortable="true" headerClass="sortable" />                        
                        <display:column property="kd_customer" title="Kd. Customer" sortable="true" headerClass="sortable" />
                        <display:column property="umur" title="Umur" sortable="true" headerClass="sortable" />                        
                        <display:column property="pria" title="Pria" sortable="true" headerClass="sortable" />
                        <display:column property="wanita" title="Wanita" sortable="true" headerClass="sortable" />
                        <display:column property="anak" title="Anak" sortable="true" headerClass="sortable" />
                        <display:column property="sepatu" title="Sepatu" sortable="true" headerClass="sortable" />
                        <display:column property="id_toko" title="ID Toko" sortable="true" headerClass="sortable" />
                        <display:setProperty name="export.excel.filename" value="Pivot.xls"/>
                    </display:table>
                </div>
                <br><br>
                <h2>Tabel Hasil Perhitungan Distance & Cluster</h2>
                <div style="width:100%; overflow-y:scroll;">
                    <display:table export="true" name="distances" id="tabDistance" class="ui celled table" cellspacing="0" sort="list" pagesize="20" requestURI="">                  
                        <display:column property="no_nota" title="No. Nota" sortable="true" headerClass="sortable" />                                                
                        <c:forEach var="dist" items="${distances[tabDistance_rowNum-1].listJarak}" varStatus="status">
                            <c:set var="counter" value="${status.index}"/>                            
                            <display:column property="listJarak[${counter}].jarak" title="${dist.no_nota}" sortable="true" headerClass="sortable" />
                        </c:forEach>
                        <display:column property="cluster" title="Cluster" sortable="true" headerClass="sortable" />                        
                        <display:setProperty name="export.excel.filename" value="Jarak&Cluster.xls"/>
                    </display:table>
                </div>
              </div>
            </div>
        </div>
    </body>
</html>
