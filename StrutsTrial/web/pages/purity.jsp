<%-- 
    Document   : purity
    Created on : Sep 13, 2016, 6:15:40 PM
    Author     : yhernanda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="simple.Fact"%>
<%@page import="simple.Pivot"%>
<%@page import="simple.Strip"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Purity</title>
       
        <link rel="stylesheet" type="text/css" href="Source/css/table.css">
        <link rel="stylesheet" type="text/css" href="Source/css/semantic.min.css">
        <link rel="stylesheet" type="text/css" href="Source/css/dataTables.semanticui.min.css">
        <script src="Source/js/jquery-2.2.3.min.js"></script>
        <script src="Source/js/semantic.min.js"></script>
        <script src="Source/js/dataTables.semanticui.min.js"></script>
        <script src="Source/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="Source/js/scatterPlot.js"></script>
        <script type="text/javascript" src="Source/js/d3.v3.min.js"></script>
    </head>
    <body>       
        <!-- Welcome, you have logined. <br />
        The attribute of 'context' in session is
        <ww:property value="#session.context" />
        <br /><br /><br />
        <a xhref="<%= request.getContextPath() %>/logout.action">Logout</a>
        <br />
        <a xhref="<%= request.getContextPath() %>/logout2.action">Logout2</a> -->
        
        <div class="ui container">
            <br>
            <div class="ui secondary menu">
              <div class="header item">
                  <i class="big home icon"></i>
                  Nearest Neighbor Clustering
              </div>
              <div class="right menu">
                <div class="item">
                  <div class="ui icon input">
                    <input type="text" placeholder="Search...">
                    <i class="search link icon"></i>
                  </div>
                </div>
                <a class="ui item" href="action/logout">
                  <i class="large user icon"></i>
                  <s:property value="#session['USER']"/>
                </a>
              </div>
            </div>
            <div class="ui divider"></div>
            <br>
            <div class="ui grid">
              <div class="four wide column">
                <div class="ui vertical pointing menu">
                  <a href="<s:url action="inputDataPage"/>" id="dashboard" class="item sidemenu">
                    <i class="large database icon"></i>
                    Input Data
                  </a>
                  <a href="<s:url action="pivotTablePage"/>"  id="inputData" class="item sidemenu">
                    <i class="large edit icon"></i>
                    Pivot Table & Distance
                  </a>
                  <a class="item active sidemenu">
                    <i class="large dashboard icon"></i>
                    Purity & Scatter Plot
                  </a>
                </div>
              </div>
              
              <!-- DASHBOARD -->
              <div id="inputData-view" class="ui grid twelve wide column">                                  
                <h2>Tabel Manual Clustering</h2>
                <div style="width:100%; overflow-y:scroll;">
                    <display:table export="true" name="manCluster" id="tabManual" class="ui celled table" cellspacing="0" sort="list" pagesize="20" requestURI="">                  
                        <display:column property="nama" title="Nama Cluster" sortable="true" headerClass="sortable" />                        
                        <display:column value="${manCluster[tabManual_rowNum-1].anggota}" title="Anggota Cluster" sortable="true" headerClass="sortable" style="word-wrap: break-word; font-size: xx-small;"/>
                        <display:column property="minUmur" title="Min Umur" sortable="true" headerClass="sortable" />                        
                        <display:column property="maxUmur" title="Max Umur" sortable="true" headerClass="sortable" />                        
                        <display:column value="${fn:length(manCluster[tabManual_rowNum-1].cluster)}" title="Jumlah" sortable="true" headerClass="sortable" />                                                                                    
                        <display:setProperty name="export.excel.filename" value="ManualCluster.xls"/>
                    </display:table>
                </div>
                <br><br>
                <h2>Tabel Detail NN Clustering</h2>
                <div style="width:100%; overflow-y:scroll;">
                    <display:table export="true" name="sysCluster" id="tabSystem" class="ui celled table" cellspacing="0" sort="list" pagesize="20" requestURI="">                  
                        <display:column property="nama" title="Nama Cluster" sortable="true" headerClass="sortable" />                                                
                        <display:column value="${sysCluster[tabSystem_rowNum-1].anggota}" title="Anggota Cluster" sortable="true" headerClass="sortable"  style="word-wrap: break-word; font-size: xx-small;"/>
                        <display:column property="dominan" title="Jumlah Data Dominan" sortable="true" headerClass="sortable" />                                                                        
                        <display:column value="${fn:length(sysCluster[tabSystem_rowNum-1].cluster)}" title="Jumlah Total Data" sortable="true" headerClass="sortable" />                                                                        
                        <display:column property="totalPria" title="P" sortable="true" headerClass="sortable" />
                        <display:column property="totalWanita" title="W" sortable="true" headerClass="sortable" />
                        <display:column property="totalAnak" title="A" sortable="true" headerClass="sortable" />
                        <display:column property="totalSepatu" title="S" sortable="true" headerClass="sortable" />
                        <display:setProperty name="export.excel.filename" value="NNCluster.xls"/>
                    </display:table>
                </div>
                <br><br>
                <h2>Purity</h2>
                <h4>Sigma Golongan/Total Data  =  <s:property value="#session['dominan']"/>/<s:property value="#session['totalData']"/>  =  <s:property value="#session['purity']"/></h4>
                <br><br>
                <h2>Scatter Plot NN Clustering</h2>
                <div id="scatter-load">
                    
                </div>
              </div>
            </div>
        </div>
    </body>
</html>
