<%-- 
    Document   : Login
    Created on : Apr 20, 2016, 12:09:22 AM
    Author     : yhernanda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        
        <link rel="stylesheet" type="text/css" href="Source/css/semantic.min.css">
        <script src="Source/js/jquery-2.2.3.min.js"></script>
        <script src="Source/js/semantic.min.js"></script>
        <script src="Source/js/form.js"></script>
        <script src="Source/js/transition.js"></script>
        
        <style type="text/css">
            body {
              background-color: #DADADA;
            }
            body > .grid {
              height: 100%;
            }
            .image {
              margin-top: -100px;
            }
            .column {
              max-width: 450px;
            }
        </style>
    </head>
    <body>
        <!--<div>
            <form action="login" method="post">
                User id<input type="text" name="userId" /> <br/>
                Password <input type="password" name="passwd" /> <br />
                <input type="submit" value="Login"/>
            </form>
        </div>-->
        
        <div class="ui middle aligned center aligned grid">
            <div class="column">
              <h2 class="ui teal image header">
                <div class="content">
                  Nearest Neighbor Clustering
                </div>
              </h2>
                
              <form action="login" method="post" class="ui large form">
                <div class="ui stacked segment">
                  <div class="field">
                    <div class="ui left icon input">
                      <i class="user icon"></i>
                      <input type="text" name="userId" placeholder="User ID">
                    </div>
                  </div>
                  <div class="field">
                    <div class="ui left icon input">
                      <i class="lock icon"></i>
                      <input type="password" name="password" placeholder="Password">
                    </div>
                  </div>
                  <input type="submit" value="login" class="ui fluid large teal submit button">
                </div>

                <div class="ui error message"></div>

              </form>

              <div class="ui message">
                New to us? <a href="#">Sign Up</a>
              </div>
            </div>
        </div>
    </body>
</html>
