<%--
    Document   : index
    Created on : Apr 19, 2016, 11:20:44 PM
    Author     : yhernanda
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="simple.Fact"%>
<%@page import="simple.Pivot"%>
<%@page import="simple.Strip"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome</title>
       
        <link rel="stylesheet" type="text/css" href="Source/css/table.css">
        <link rel="stylesheet" type="text/css" href="Source/css/semantic.min.css">
        <link rel="stylesheet" type="text/css" href="Source/css/dataTables.semanticui.min.css">
        <script src="Source/js/jquery-2.2.3.min.js"></script>
        <script src="Source/js/semantic.min.js"></script>
        <!--<script src="Source/js/dataTables.semanticui.min.js"></script>-->
        <script src="Source/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="Source/js/welcome.js"></script>
    </head>
    <body>       
        <!-- Welcome, you have logined. <br />
        The attribute of 'context' in session is
        <ww:property value="#session.context" />
        <br /><br /><br />
        <a xhref="<%= request.getContextPath() %>/logout.action">Logout</a>
        <br />
        <a xhref="<%= request.getContextPath() %>/logout2.action">Logout2</a> -->
        
        <div class="ui container">
            <br>
            <div class="ui secondary menu">
              <div class="header item">
                  <i class="big home icon"></i>
                  Nearest Neighbor Clustering
              </div>
              <div class="right menu">
                <div class="item">
                  <div class="ui icon input">
                    <input type="text" placeholder="Search...">
                    <i class="search link icon"></i>
                  </div>
                </div>
                <a class="ui item" href="<s:url action="logout"/>">
                  <i class="large user icon"></i>
                  <s:property value="#session['USER']"/>
                </a>
              </div>
            </div>
            <div class="ui divider"></div>
            <br>
            <div class="ui grid">
              <div class="four wide column">
                <div class="ui vertical pointing menu">
                  <a href="<s:url action="inputDataPage"/>" id="dashboard" class="item active sidemenu">
                    <i class="large database icon"></i>
                    Input Data
                  </a>
                  <a href="<s:url action="pivotTablePage"/>" id="inputData" class="item sidemenu">
                    <i class="large edit icon"></i>
                    Pivot Table & Distance
                  </a>
                  <a href="<s:url action="purityPage"/>" class="item sidemenu">
                    <i class="large dashboard icon"></i>
                    Purity & Scatter Plot
                  </a>                  
                </div>
              </div>
              
              <!-- DASHBOARD -->
              <div id="inputData-view" class="ui grid twelve wide column">
                <form action="displayFactList" method="post" class="ui form">              
                    <div class="three fields">
                      <div class="field">
                        <label>Start date</label>
                        <div class="ui calendar" id="rangestart">
                          <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="Start" name="startDate" id="startDate">
                          </div>
                        </div>
                      </div>
                      <div class="field">
                        <label>End date</label>
                        <div class="ui calendar" id="rangeend">
                          <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="End" name="endDate">
                          </div>
                        </div>
                      </div>
                      <div class="field" style="display: flex; justify-content:bottom; align-items:center;">                         
                          <input type="submit" value="Get Data" class="ui fluid teal submit button">  
                      </div>
                    </div>                    
                    <div class="three fields">
                        <div class="field"></div>
                        <div class="field">
                            <select id="selectNormalisasi">
                                <option value="<s:url action='minMax'/>" selected>MinMax Normalization</option>
                                <option value="<s:url action='zScore'/>">Z-Score Normalization</option>
                            </select>
                        </div>
                        <div class="field">
                            <a id="btnNormalisasi" href="<s:url action='minMax'/>" class="ui fluid teal submit button">Normalisasi</a>
                        </div>                                                
                    </div>
                </form>                
                <br>
                <br>
                <h2>Tabel Fakta</h2>
                <display:table export="true" name="factList" id="tabMentah" class="ui celled table" cellspacing="0" sort="list" pagesize="20" requestURI="">
                    <display:column property="tanggal" title="Tanggal" sortable="true" headerClass="sortable" />
                    <display:column property="no_nota" title="No. Nota" sortable="true" headerClass="sortable" />
                    <display:column property="kd_customer" title="Kd. Customer" sortable="true" headerClass="sortable" />
                    <display:column property="umur" title="Umur" sortable="true" headerClass="sortable" />
                    <display:column property="kel_jns" title="Kel. Jenis" sortable="true" headerClass="sortable" />
                    <display:column property="jumlah" title="Jumlah" sortable="true" headerClass="sortable" />                                                            
                    <display:column property="id_toko" title="ID Toko" sortable="true" headerClass="sortable" />
                    <display:setProperty name="export.excel.filename" value="Fakta.xls"/>
                </display:table>
                <br><br>
                <h2 id="titleNormalisasi">Tabel Hasil Normalisasi</h2>
                <display:table export="true" name="normalizedFact" id="tabNormalisasi" class="ui celled table" cellspacing="0" sort="list" pagesize="20" requestURI="">
                    <display:column property="tanggal" title="Tanggal" sortable="true" headerClass="sortable" />
                    <display:column property="no_nota" title="No. Nota" sortable="true" headerClass="sortable" />
                    <display:column property="kd_customer" title="Kd. Customer" sortable="true" headerClass="sortable" />
                    <display:column property="umur" title="Umur" sortable="true" headerClass="sortable" />
                    <display:column property="kel_jns" title="Kel. Jenis" sortable="true" headerClass="sortable" />
                    <display:column property="jumlah" title="Jumlah" sortable="true" headerClass="sortable" />                                                            
                    <display:column property="id_toko" title="ID Toko" sortable="true" headerClass="sortable" />
                    <display:setProperty name="export.excel.filename" value="Normalisasi.xls"/>
                </display:table>                
                <!--<table id="tabCoba" class="ui celled table" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                          <th>No. Nota</th>
                          <th>Kd. Strip</th>
                          <th>Jumlah</th>
                          <th>Tanggal</th>
                          <th>Kd. Customer</th>
                          <th>ID Department</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                          <th>No. Nota</th>
                          <th>Kd. Strip</th>
                          <th>Jumlah</th>
                          <th>Tanggal</th>
                          <th>Kd. Customer</th>
                          <th>ID Department</th>
                      </tr>
                    </tfoot>
                    <tbody>
                      
                    </tbody>
                </table>-->
                
              </div>
            </div>
        </div>
    </body>
</html>