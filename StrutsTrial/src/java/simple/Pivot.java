/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author yhernanda
 */
public class Pivot {
    
    public Pivot(String no_nota, Date tanggal, String kd_customer, int umur, int id_department, List<Strip> kd_strip) throws ClassNotFoundException, SQLException{
        this.kd_strip = kd_strip;
        this.no_nota = no_nota;
        this.tanggal = tanggal;
        this.kd_customer = kd_customer;
        this.umur = umur;
        this.id_department = id_department;
    }
    
    public Pivot(){
    
    }
    
    private String no_nota;
    private Date tanggal;
    private String kd_customer;
    private int umur;
    private int id_department;
    private List<Strip> kd_strip = new ArrayList<Strip>();
    private int cluster;

    /**
     * @return the no_nota
     */
    public String getNo_nota() {
        return no_nota;
    }

    /**
     * @param no_nota the no_nota to set
     */
    public void setNo_nota(String no_nota) {
        this.no_nota = no_nota;
    }

    /**
     * @return the tanggal
     */
    public Date getTanggal() {
        return tanggal;
    }

    /**
     * @param tanggal the tanggal to set
     */
    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    /**
     * @return the kd_customer
     */
    public String getKd_customer() {
        return kd_customer;
    }

    /**
     * @param kd_customer the kd_customer to set
     */
    public void setKd_customer(String kd_customer) {
        this.kd_customer = kd_customer;
    }

    /**
     * @return the id_department
     */
    public int getId_department() {
        return id_department;
    }

    /**
     * @param id_department the id_department to set
     */
    public void setId_department(int id_department) {
        this.id_department = id_department;
    }

    /**
     * @return the kd_strip
     */
    public List<Strip> getKd_strip() {
        return kd_strip;
    }

    /**
     * @param kd_strip the kd_strip to set
     */
    public void setKd_strip(List<Strip> kd_strip) {
        this.kd_strip = kd_strip;
    }
    
    public int getKd_strip_size() {
        return kd_strip.size();
    }

    /**
     * @return the cluster
     */
    public int getCluster() {
        return cluster;
    }

    /**
     * @param cluster the cluster to set
     */
    public void setCluster(int cluster) {
        this.cluster = cluster;
    }

    /**
     * @return the umur
     */
    public int getUmur() {
        return umur;
    }

    /**
     * @param umur the umur to set
     */
    public void setUmur(int umur) {
        this.umur = umur;
    }
}
