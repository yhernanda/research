/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JOptionPane;
import org.apache.struts2.interceptor.SessionAware;


/**
 *
 * @author yhernanda
 */
public class LoginAction extends ActionSupport{
 
    private String userId;
    private String password;
    private String name;
    private HiveConnector hc = new HiveConnector();
    ArrayList ar = new ArrayList();
    /*public String execute() throws Exception {
        if ("admin".equals(userId) && "password".equals(passwd)) {
//            HttpSession session = ServletActionContext.getRequest().getSession();
//            session.setAttribute("logined","true");
//            session.setAttribute("context", new Date());
// Better is using ActionContext 
  Map session = ActionContext.getContext().getSession();
session.put("logined","true");
            session.put("context", new Date());
            return SUCCESS;
        }
        return ERROR;
    }
    
    public String logout() throws Exception {
//        HttpSession session = ServletActionContext.getRequest().getSession();
//        session.removeAttribute("logined");
//        session.removeAttribute("context"); 
 Map session = ActionContext.getContext().getSession();
 session.remove("logined");
        session.remove("context");
        return SUCCESS;
    }
    */
    
    public String execute() {
        String ret = ERROR;
        Connection conn = null;

        try {           
           //String URL = "jdbc:mariadb://localhost:3306/amigoView";
           //Class.forName("org.mariadb.jdbc.Driver");
           //conn = DriverManager.getConnection(URL, "root", "mariadbroot");
           conn = hc.getConn();
           String sql = "SELECT `name` FROM `user` WHERE `username` = ? AND `password` = ?";
           PreparedStatement ps = conn.prepareStatement(sql);
           ps.setString(1, userId);
           ps.setString(2, password);
           System.out.println(ps.toString());
           ResultSet rs = ps.executeQuery();
           while (rs.next()) {
              name = rs.getString("name");
              ret = SUCCESS;
           }
           
           Map<String, Object> session = ActionContext.getContext().getSession();
           session.clear();
           session.put("USER", name);
        } catch (Exception e) {
           ret = ERROR;
        } finally {
           if (conn != null) {
              try {
                 conn.close();
              } catch (Exception e) {
              }
           }
        }
        System.gc();
        return ret;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
 
    public String getUserId() {
        return userId;
    }
 
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    private Map<String, Object> session;
    public void setSession(Map<String, Object> session){
        this.session = session;
    }
}