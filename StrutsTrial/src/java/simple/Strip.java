/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

/**
 *
 * @author yhernanda
 */
public class Strip {
    
    public Strip(String kd_strip, Double jumlah){
        this.kd_strip = kd_strip;
        this.jumlah = jumlah;
    }
    
    public Strip(){
        
    }
    
    private String kd_strip;
    private Double jumlah;

    /**
     * @return the kd_strip
     */
    public String getKd_strip() {
        return kd_strip;
    }

    /**
     * @param kd_strip the kd_strip to set
     */
    public void setKd_strip(String kd_strip) {
        this.kd_strip = kd_strip;
    }

    /**
     * @return the jumlah
     */
    public Double getJumlah() {
        return jumlah;
    }

    /**
     * @param jumlah the jumlah to set
     */
    public void setJumlah(Double jumlah) {
        this.jumlah = jumlah;
    }
}
