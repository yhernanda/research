/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import simple.PivotJenis;

/**
 *
 * @author yhernanda
 */
public class ManualCluster implements Serializable {
    private String nama;
    private String anggota;   
    private int minUmur;
    private int maxUmur;
    private String jenis;
    private List<PivotJenis> cluster;
    
    public ManualCluster(){
        this.nama = "";
        this.minUmur = 0;
        this.maxUmur = 0;
        this.jenis = "";
        this.cluster = new ArrayList<PivotJenis>();
    }
    
    public ManualCluster(String nama, int minUmur, int maxUmur, String jenis){
        this.nama = nama;
        this.minUmur = minUmur;
        this.maxUmur = maxUmur;
        this.jenis = jenis;
        this.cluster = new ArrayList<PivotJenis>();
    }
    
    public String getAnggota(){
        anggota = "";
        for (int i = 0; i < cluster.size(); i++) {
            anggota += cluster.get(i).getNo_nota();
            if(i != cluster.size()-1)
                anggota += ", ";
        }
        return anggota;
    }

    /**
     * @return the nama
     */
    public String getNama() {
        return nama;
    }

    /**
     * @param nama the nama to set
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     * @return the minUmur
     */
    public int getMinUmur() {
        return minUmur;
    }

    /**
     * @param minUmur the minUmur to set
     */
    public void setMinUmur(int minUmur) {
        this.minUmur = minUmur;
    }

    /**
     * @return the maxUmur
     */
    public int getMaxUmur() {
        return maxUmur;
    }

    /**
     * @param maxUmur the maxUmur to set
     */
    public void setMaxUmur(int maxUmur) {
        this.maxUmur = maxUmur;
    }

    /**
     * @return the jenis
     */
    public String getJenis() {
        return jenis;
    }

    /**
     * @param jenis the jenis to set
     */
    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    /**
     * @return the cluster
     */
    public List<PivotJenis> getCluster() {
        return cluster;
    }

    /**
     * @param cluster the cluster to set
     */
    public void setCluster(List<PivotJenis> cluster) {
        this.cluster = cluster;
    }


}
