/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author yhernanda
 */
public class HiveConnector {
    private Connection conn;
    private String driverName = "org.apache.hive.jdbc.HiveDriver";
    //private String host = "jdbc:hive2://192.168.1.100:10000/yoas";
    private String host = "jdbc:hive2://localhost:10000/amigo";
    private String user = "hive";
    private String pass = "";
    
    public HiveConnector(){
        try{
            Class.forName(this.driverName);
            this.conn = DriverManager.getConnection(host, user, pass);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    
    public Connection getConn(){
        return conn;
    }
}