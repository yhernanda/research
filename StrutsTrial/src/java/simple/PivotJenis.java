/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import java.io.Serializable;
import java.sql.Date;
import java.util.Comparator;
import static jodd.format.Printf.str;

/**
 *
 * @author yhernanda
 */
public class PivotJenis implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String no_nota;
    private Date tanggal;
    private String kd_customer;
    private double umur;
    private int id_toko;
    private double pria;
    private double wanita;
    private double anak;
    private double sepatu;
    private double x;
    private double y;
    private int cluster;    
    
    public PivotJenis(String no_nota, Date tanggal, String kd_customer, Double umur, double pria, double wanita, double anak, double sepatu, int id_toko){        
        this.no_nota = no_nota;
        this.tanggal = tanggal;
        this.kd_customer = kd_customer;
        this.umur = umur;        
        this.pria = pria;
        this.wanita = wanita;
        this.anak = anak;
        this.sepatu = sepatu;
        this.id_toko = id_toko;
    }
    
    public PivotJenis(PivotJenis p){
        this.no_nota = p.getNo_nota();
        this.tanggal = p.getTanggal();
        this.kd_customer = p.getKd_customer();
        this.umur = p.getUmur();        
        this.pria = p.getPria();
        this.wanita = p.getWanita();
        this.anak = p.getAnak();
        this.sepatu = p.getSepatu();
        this.id_toko = p.getId_toko();
        this.cluster = p.getCluster();
        this.x = p.getX();
        this.y = p.getY();
    }
    
    public PivotJenis(){
        
    }

    /**
     * @return the no_nota
     */
    public String getNo_nota() {
        return no_nota;
    }

    /**
     * @param no_nota the no_nota to set
     */
    public void setNo_nota(String no_nota) {
        this.no_nota = no_nota;
    }

    /**
     * @return the tanggal
     */
    public Date getTanggal() {
        return tanggal;
    }

    /**
     * @param tanggal the tanggal to set
     */
    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    /**
     * @return the kd_customer
     */
    public String getKd_customer() {
        return kd_customer;
    }

    /**
     * @param kd_customer the kd_customer to set
     */
    public void setKd_customer(String kd_customer) {
        this.kd_customer = kd_customer;
    }

    /**
     * @return the pria
     */
    public double getPria() {
        return pria;
    }

    /**
     * @param pria the pria to set
     */
    public void setPria(double pria) {
        this.pria = pria;
    }

    /**
     * @return the wanita
     */
    public double getWanita() {
        return wanita;
    }

    /**
     * @param wanita the wanita to set
     */
    public void setWanita(double wanita) {
        this.wanita = wanita;
    }

    /**
     * @return the anak
     */
    public double getAnak() {
        return anak;
    }

    /**
     * @param anak the anak to set
     */
    public void setAnak(double anak) {
        this.anak = anak;
    }

    /**
     * @return the sepatu
     */
    public double getSepatu() {
        return sepatu;
    }

    /**
     * @param sepatu the sepatu to set
     */
    public void setSepatu(double sepatu) {
        this.sepatu = sepatu;
    }

    /**
     * @return the cluster
     */
    public int getCluster() {
        return cluster;
    }

    /**
     * @param cluster the cluster to set
     */
    public void setCluster(int cluster) {
        this.cluster = cluster;
    }

    /**
     * @return the umur
     */
    public double getUmur() {
        return umur;
    }

    /**
     * @param umur the umur to set
     */
    public void setUmur(double umur) {
        this.umur = umur;
    }      

    /**
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * @return the id_toko
     */
    public int getId_toko() {
        return id_toko;
    }

    /**
     * @param id_toko the id_toko to set
     */
    public void setId_toko(int id_toko) {
        this.id_toko = id_toko;
    }
    
    @Override
    public String toString() {
        String str = "{'no_nota' : '"+ no_nota +"', 'id_customer' : '"+ kd_customer +"', 'umur' : "+ umur +"}";
        return str;                
    }
}
