/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import java.io.Serializable;

/**
 *
 * @author yhernanda
 */
public class NodeDist implements Serializable {
    
    public NodeDist(){
        
    }
    
    public NodeDist(String no_nota, Double jarak){
        this.no_nota = no_nota;
        this.jarak = jarak;
    }
    
    private String no_nota;
    private double jarak;

    /**
     * @return the no_nota
     */
    public String getNo_nota() {
        return no_nota;
    }

    /**
     * @param no_nota the no_nota to set
     */
    public void setNo_nota(String no_nota) {
        this.no_nota = no_nota;
    }

    /**
     * @return the jarak
     */
    public Double getJarak() {
        return jarak;
    }

    /**
     * @param jarak the jarak to set
     */
    public void setJarak(Double jarak) {
        this.jarak = jarak;
    }
}
