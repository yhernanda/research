/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

/**
 *
 * @author yhernanda
 */
public class StripJenis {
    private String kd_strip;
    private String kel_jns;
    
    public StripJenis(){
        
    }
    
    public StripJenis(String kd_strip, String kel_jns){
        this.kd_strip = kd_strip;
        this.kel_jns = kel_jns;
    }

    /**
     * @return the kd_strip
     */
    public String getKd_strip() {
        return kd_strip;
    }

    /**
     * @param kd_strip the kd_strip to set
     */
    public void setKd_strip(String kd_strip) {
        this.kd_strip = kd_strip;
    }

    /**
     * @return the kel_jns
     */
    public String getKel_jns() {
        return kel_jns;
    }

    /**
     * @param kel_jns the kel_jns to set
     */
    public void setKel_jns(String kel_jns) {
        this.kel_jns = kel_jns;
    }
}
