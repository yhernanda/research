/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author yhernanda
 */
public class Fact implements Comparable, Serializable{
    
    public Fact(String no_nota, String kel_jns, Double jumlah, Date tanggal, String kd_customer, Double umur, int id_toko){
        this.setNo_nota(no_nota);
        this.setKel_jns(kel_jns);
        this.setJumlah(jumlah);
        this.setTanggal(tanggal);
        this.setKd_customer(kd_customer);
        this.setUmur(umur);
        this.setId_toko(id_toko);
    }
    
    public Fact(){
    
    }
    
    private String no_nota;
    private String kel_jns; 
    private Double jumlah;
    private Date tanggal;
    private String kd_customer;
    private Double umur;
    private int id_toko;

    /**
     * @return the no_nota
     */
    public String getNo_nota() {
        return no_nota;
    }

    /**
     * @param no_nota the no_nota to set
     */
    public void setNo_nota(String no_nota) {
        this.no_nota = no_nota;
    }

    /**
     * @return the jumlah
     */
    public Double getJumlah() {
        return jumlah;
    }

    /**
     * @param jumlah the jumlah to set
     */
    public void setJumlah(Double jumlah) {
        this.jumlah = jumlah;
    }

    /**
     * @return the tanggal
     */
    public Date getTanggal() {
        return tanggal;
    }

    /**
     * @param tanggal the tanggal to set
     */
    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    /**
     * @return the kd_customer
     */
    public String getKd_customer() {
        return kd_customer;
    }

    /**
     * @param kd_customer the kd_customer to set
     */
    public void setKd_customer(String kd_customer) {
        this.kd_customer = kd_customer;
    }

   @Override
    public int compareTo(Object otherFact) {        
        if (!(otherFact instanceof Fact))
            throw new ClassCastException("A Person object expected.");        
        if(getJumlah() < ((Fact) otherFact).getJumlah()){
            otherFact=null;
            return -1;
        }
        if(getJumlah() > ((Fact) otherFact).getJumlah()){
            otherFact=null;
            return 1;
        }
        otherFact=null;
        return 0;
     //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the umur
     */
    public Double getUmur() {
        return umur;
    }

    /**
     * @param umur the umur to set
     */
    public void setUmur(Double umur) {
        this.umur = umur;
    }

    /**
     * @return the kel_jns
     */
    public String getKel_jns() {
        return kel_jns;
    }

    /**
     * @param kel_jns the kel_jns to set
     */
    public void setKel_jns(String kel_jns) {
        this.kel_jns = kel_jns;
    }

    /**
     * @return the id_toko
     */
    public int getId_toko() {
        return id_toko;
    }

    /**
     * @param id_toko the id_toko to set
     */
    public void setId_toko(int id_toko) {
        this.id_toko = id_toko;
    }
}