/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.commons.lang.ArrayUtils;
import org.apache.hive.jdbc.HiveStatement;
import simple.Fact;
import simple.Pivot;
import simple.PivotJenis;
import simple.Strip;
import com.opensymphony.xwork2.Action;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.RoundingMode;
import java.text.DecimalFormat;


/**
 *
 * @author yhernanda
 */
public class FetchData extends ActionSupport implements Serializable{
    private static final long serialVersionUID = 1L;
    private Map<String, Object> session = ActionContext.getContext().getSession();
    private static Connection connection = null;
    private String startDate;
    private String endDate;    
    private PivotJenis pjTemp;
    private double threshold = 0.0;
    private int lastCluster = 1;
    private List<Fact> factList=new ArrayList<Fact>();
    private List<Fact> Facts=new ArrayList<Fact>();
    private List<Fact> normalizedFact=new ArrayList<Fact>();
    private List<Pivot> pivotList=new ArrayList<Pivot>();
    private List<PivotJenis> pivotListJenis=new ArrayList<PivotJenis>();
    private List<Distance> distances=new ArrayList<Distance>();
    private List<ManualCluster> manCluster=new ArrayList<ManualCluster>();
    private List<SystemCluster> sysCluster=new ArrayList<SystemCluster>();    
    private HashMap <String, PivotTemp> pivotTemp=new HashMap<String, PivotTemp>();
    private HashMap <String, Integer> umurTemp=new HashMap<String, Integer>();
    private HashMap <String, Integer> jumlahJns=new HashMap<String, Integer>();    
    private ResultSet flist;
    private ResultSet plist;
    private ResultSet striplist;
    private PreparedStatement ps;
    private HiveConnector hc = new HiveConnector();
    private MySQLConnector mc = new MySQLConnector();
    
    //PAGE
    public String inputDataPage() throws SQLException, IOException, ClassNotFoundException{
        if(getData("tabelFakta")!=null)
            factList = new ArrayList<Fact>((List<Fact>) getData("tabelFakta"));
        if(getData("dataNormal")!=null)
            normalizedFact = new ArrayList<Fact>((List<Fact>) getData("dataNormal"));
        
        return SUCCESS;
    }
    
    public String pivotTablePage() throws SQLException, ClassNotFoundException, IOException{
        if(getData("dataPivot")!=null){
            //JOptionPane.showMessageDialog(null, "pivotPageif");
            pivotListJenis = new ArrayList<PivotJenis>((List<PivotJenis>) getData("dataPivot"));
            
            if(getSession().get("dataJarak")!=null)
                distances=new ArrayList<Distance>((List<Distance>) getSession().get("dataJarak"));
        
            if(getSession().get("systemCluster")!=null)
                sysCluster=new ArrayList<SystemCluster>((List<SystemCluster>) getSession().get("systemCluster"));
        }            
        else{
            //JOptionPane.showMessageDialog(null, "pivotPageelse");
            pivotTableHiveNew();
        }                            
        
        return SUCCESS;
    }
    
    public String purityPage() throws SQLException, IOException, ClassNotFoundException{
        if(getSession().get("manualCluster")!=null)
            sysCluster = new ArrayList<SystemCluster>((List<SystemCluster>) getSession().get("manualCluster"));
        if(getSession().get("systemCluster")!=null)
            sysCluster = new ArrayList<SystemCluster>((List<SystemCluster>) getSession().get("systemCluster"));
        if(getData("umurAsli")!=null)
            umurTemp = new HashMap<String, Integer>((HashMap<String, Integer>) getData("UMURASLI"));
        
        return SUCCESS;
    }
    
    
    
    //ZScore
    public String zScoreFactList() throws SQLException, ClassNotFoundException, IOException{        
        String str = (String) getSession().get("USER");
        getSession().clear();
        getSession().put("USER", str);
        startDate = (String) getData("startDate");
        endDate = (String) getData("endDate");
        jumlahJns = (HashMap<String, Integer>) getData("jumlahAsli");
        umurTemp = (HashMap<String, Integer>) getData("umurAsli");
        
        factList = new ArrayList<Fact>((List<Fact>) getData("tabelFakta"));
        Facts = new ArrayList<Fact>((List<Fact>) getData("normalisasi"));
        normalizedFact = new ArrayList<Fact>((List<Fact>) getData("normalisasi"));
        str = null;clearData();        
        HashSet<String> notaTemp = new HashSet<String>();
        
        double meanPria = 0.0, meanWanita = 0.0, meanAnak = 0.0, meanSepatu = 0.0, meanUmur = 0.0, temp = 0.0;
        int jumlahPria = 0, jumlahWanita = 0, jumlahAnak = 0, jumlahSepatu = 0, jumlahUmur = 0;
        for (int i = 0; i < normalizedFact.size(); i++) {
            if(normalizedFact.get(i).getKel_jns().equals("P")){
                meanPria += normalizedFact.get(i).getJumlah();
                jumlahPria++;
            }else if(normalizedFact.get(i).getKel_jns().equals("W")){
                meanWanita += normalizedFact.get(i).getJumlah();
                jumlahWanita++;
            }else if(normalizedFact.get(i).getKel_jns().equals("A")){
                meanAnak += normalizedFact.get(i).getJumlah();
                jumlahAnak++;
            }else if(normalizedFact.get(i).getKel_jns().equals("S"))                {
                meanSepatu += normalizedFact.get(i).getJumlah();
                jumlahSepatu++;
            }
            if(notaTemp.add(normalizedFact.get(i).getNo_nota())){
                meanUmur += normalizedFact.get(i).getUmur();
                jumlahUmur++;
            }                
        }
        meanPria = meanPria/jumlahPria;        
        meanWanita = meanWanita/jumlahWanita;
        meanAnak = meanAnak/jumlahAnak;
        meanSepatu = meanSepatu/jumlahSepatu;
        meanUmur = meanUmur/jumlahUmur;
        notaTemp=null;
        notaTemp=new HashSet<String>();
        double stdPria = 0.0, stdWanita = 0.0, stdAnak = 0.0, stdSepatu = 0.0, stdUmur = 0.0;        
        for (int i = 0; i < normalizedFact.size(); i++){
            if(normalizedFact.get(i).getKel_jns().equals("P")){
                stdPria += Math.pow((normalizedFact.get(i).getJumlah() - meanPria), 2);
            }else if(normalizedFact.get(i).getKel_jns().equals("W")){
                stdWanita += Math.pow((normalizedFact.get(i).getJumlah() - meanWanita), 2);
            }else if(normalizedFact.get(i).getKel_jns().equals("A")){
                stdAnak += Math.pow((normalizedFact.get(i).getJumlah() - meanAnak), 2);
            }else if(normalizedFact.get(i).getKel_jns().equals("S")){
                stdSepatu += Math.pow((normalizedFact.get(i).getJumlah() - meanSepatu), 2);
            }
            if(notaTemp.add(normalizedFact.get(i).getNo_nota())){
                stdUmur += Math.pow((normalizedFact.get(i).getUmur() - meanUmur), 2);
            }            
        }                
        stdPria = Math.sqrt(stdPria/jumlahPria);        
        stdWanita = Math.sqrt(stdWanita/jumlahWanita);
        stdAnak = Math.sqrt(stdAnak/jumlahAnak);
        stdSepatu = Math.sqrt(stdSepatu/jumlahSepatu);
        stdUmur = Math.sqrt(stdUmur/jumlahUmur);      
        for (int i = 0; i < normalizedFact.size(); i++) {            
            if(normalizedFact.get(i).getKel_jns().equals("P")){
                temp = (normalizedFact.get(i).getJumlah()-meanPria)/stdPria;                            
                //System.out.println(normalizedFact.get(i).getJumlah()+"-"+meanPria+"/"+stdPria+"="+temp);
            }else if(normalizedFact.get(i).getKel_jns().equals("W")){
                temp = (normalizedFact.get(i).getJumlah()-meanWanita)/stdWanita;                
                //System.out.println(normalizedFact.get(i).getJumlah()+"-"+meanWanita+"/"+stdWanita+"="+temp);
            }else if(normalizedFact.get(i).getKel_jns().equals("A")){
                temp = (normalizedFact.get(i).getJumlah()-meanAnak)/stdAnak;                
                //System.out.println(normalizedFact.get(i).getJumlah()+"-"+meanAnak+"/"+stdAnak+"="+temp);
            }else if(normalizedFact.get(i).getKel_jns().equals("S")){
                temp = (normalizedFact.get(i).getJumlah()-meanSepatu)/stdSepatu;                                                              
                //System.out.println(normalizedFact.get(i).getJumlah()+"-"+meanSepatu+"/"+stdSepatu+"="+temp);
            }
            if(Double.isNaN(temp))
                    temp = 0;
            getNormalizedFact().get(i).setJumlah(temp);
            getNormalizedFact().get(i).setUmur((normalizedFact.get(i).getUmur()-meanUmur)/stdUmur);
            pivotTemp.put(getNormalizedFact().get(i).getNo_nota()+"_"+getNormalizedFact().get(i).getKel_jns(), new PivotTemp(temp,normalizedFact.get(i).getUmur()));            
        }
        saveData("jumlahAsli", jumlahJns);
        saveData("umurAsli", umurTemp);            
        saveData("startDate", startDate);
        saveData("endDate", endDate);
        saveData("tabelFakta", new ArrayList<Fact>(factList));
        
        saveData("normalisasi", new ArrayList<Fact>(Facts));
        saveData("dataNormal", new ArrayList<Fact>(normalizedFact));
        saveData("pivoting", pivotTemp);        
        //pivotTableHiveNew();
        Facts=null;
        System.gc();
        return SUCCESS;
    }
    
    //MinMax Normalization
    public String minMaxFactList() throws SQLException, ClassNotFoundException, IOException{
        //JOptionPane.showMessageDialog(null, "MINMAX");
        String str = (String) getSession().get("USER");
        getSession().clear();
        getSession().put("USER", str);
        startDate = (String) getData("startDate");
        endDate = (String) getData("endDate");
        jumlahJns = (HashMap<String, Integer>) getData("jumlahAsli");
        umurTemp = (HashMap<String, Integer>) getData("umurAsli");
        
        factList = new ArrayList<Fact>((List<Fact>) getData("tabelFakta"));
        Facts = new ArrayList<Fact>((List<Fact>) getData("normalisasi"));
        normalizedFact = new ArrayList<Fact>((List<Fact>) getData("normalisasi"));
        str = null;clearData();
                        
        double minUmur=0.0, minPria=0.0, minWanita=0.0, minAnak=0.0, minSepatu=0.0;
        double maxUmur=0.0, maxPria=0.0, maxWanita=0.0, maxAnak=0.0, maxSepatu=0.0;
        for (int i = 0; i < getNormalizedFact().size(); i++) {
            if(normalizedFact.get(i).getKel_jns().equals("P")){                
                if(normalizedFact.get(i).getJumlah() > maxPria)
                    maxPria = normalizedFact.get(i).getJumlah();
                if(normalizedFact.get(i).getJumlah() < minPria || minPria==0.0)
                    minPria = normalizedFact.get(i).getJumlah();
            }else if(normalizedFact.get(i).getKel_jns().equals("W")){
                if(normalizedFact.get(i).getJumlah() > maxWanita)
                    maxWanita = normalizedFact.get(i).getJumlah();
                if(normalizedFact.get(i).getJumlah() < minWanita || minWanita==0.0)
                    minWanita = normalizedFact.get(i).getJumlah();
            }else if(normalizedFact.get(i).getKel_jns().equals("A")){
                if(normalizedFact.get(i).getJumlah() > maxAnak)
                    maxAnak = normalizedFact.get(i).getJumlah();
                if(normalizedFact.get(i).getJumlah() < minAnak || minAnak==0.0)
                    minAnak = normalizedFact.get(i).getJumlah();
            }else if(normalizedFact.get(i).getKel_jns().equals("S")){
                if(normalizedFact.get(i).getJumlah() > maxSepatu)
                    maxSepatu = normalizedFact.get(i).getJumlah();
                if(normalizedFact.get(i).getJumlah() < minSepatu || minSepatu==0.0)
                    minSepatu = normalizedFact.get(i).getJumlah();
            }
            if(normalizedFact.get(i).getUmur() > maxUmur)
                maxUmur = normalizedFact.get(i).getUmur();
            if(normalizedFact.get(i).getUmur() < minUmur || minUmur==0.0)
                minUmur = normalizedFact.get(i).getUmur();
        }
        for (int i = 0; i < getNormalizedFact().size(); i++) {
            double jumlahAwal = getNormalizedFact().get(i).getJumlah();
            if(normalizedFact.get(i).getKel_jns().equals("P"))
                jumlahAwal = ((jumlahAwal - minPria)/(maxPria - minPria))*1;
            else if(normalizedFact.get(i).getKel_jns().equals("W"))
                jumlahAwal = ((jumlahAwal - minWanita)/(maxWanita - minWanita))*1;
            else if(normalizedFact.get(i).getKel_jns().equals("A"))
                jumlahAwal = ((jumlahAwal - minAnak)/(maxAnak - minAnak))*1;
            else if(normalizedFact.get(i).getKel_jns().equals("S"))
                jumlahAwal = ((jumlahAwal - minSepatu)/(maxSepatu - minSepatu))*1;
            if(Double.isNaN(jumlahAwal))
                jumlahAwal = 0;
            getNormalizedFact().get(i).setJumlah(jumlahAwal);
            getNormalizedFact().get(i).setUmur(((getNormalizedFact().get(i).getUmur() - minUmur)/(maxUmur - minUmur))*1);
            pivotTemp.put(getNormalizedFact().get(i).getNo_nota()+"_"+getNormalizedFact().get(i).getKel_jns(), new PivotTemp(jumlahAwal,normalizedFact.get(i).getUmur()));            
        }
        //System.out.println(minUmur+" U "+maxUmur+" | "+minPria+" P "+maxPria+" | "+minWanita+" W "+maxWanita+" | "+minAnak+" A "+maxAnak+" | "+minSepatu+" S "+maxSepatu);
        saveData("jumlahAsli", jumlahJns);
        saveData("umurAsli", umurTemp);            
        saveData("startDate", startDate);
        saveData("endDate", endDate);
        saveData("tabelFakta", new ArrayList<Fact>(factList));
        
        saveData("normalisasi", new ArrayList<Fact>(Facts));
        saveData("dataNormal", new ArrayList<Fact>(normalizedFact));
        saveData("pivoting", pivotTemp);
        
        Facts=null;
        System.gc();
        return SUCCESS;
    }
    
    //Pivot 5 atribut (Umur, P, W, A, S)
    public String pivotTableHiveNew() throws SQLException, ClassNotFoundException, IOException{
        //JOptionPane.showMessageDialog(null, "PIVOT");
        factList = (List<Fact>) getData("tabelFakta");
        normalizedFact = (List<Fact>) getData("dataNormal");
        pivotTemp = (HashMap<String, PivotTemp>) getData("pivoting");
              
        String str = null;         
        Strip strip;
        PivotJenis pivot;        
        Set<String> hs = new HashSet<>();        
        
        connection = hc.getConn();
        //str = "select no_bon, tanggal, kd_customer, id_toko from `bon_fact_jenis` where tanggal between '"+ getData("startDate") +"' and '"+ getData("endDate") +"'";        
        str = "select no_nota, tanggal, kd_customer, id_toko from `tunai_fact_jenis` where tanggal between '"+ getData("startDate") +"' and '"+ getData("endDate") +"'";        
        ps = connection.prepareStatement(str);
        plist = ps.executeQuery();        
        ps.clearParameters();                             
                       
        while(plist.next()){                
            if(hs.add(plist.getString(1))){                                
                pivot = new PivotJenis(plist.getString(1), plist.getDate(2), plist.getString(3), 0.0, 0.0, 0.0, 0.0, 0.0, plist.getInt(4));                                
                boolean setUmur = false;
                
                if(pivotTemp.get(plist.getString(1)+"_P") != null){
                    pivot.setPria(pivotTemp.get(plist.getString(1)+"_P").getJumlah());
                    if(setUmur==false){
                        pivot.setUmur(pivotTemp.get(plist.getString(1)+"_P").getUmur());
                        setUmur=true;
                    }                    
                }
                if(pivotTemp.get(plist.getString(1)+"_W") != null){
                    pivot.setWanita(pivotTemp.get(plist.getString(1)+"_W").getJumlah());
                    if(setUmur==false){
                        pivot.setUmur(pivotTemp.get(plist.getString(1)+"_W").getUmur());
                        setUmur=true;
                    }
                }
                if(pivotTemp.get(plist.getString(1)+"_A") != null){
                    pivot.setAnak(pivotTemp.get(plist.getString(1)+"_A").getJumlah());
                    if(setUmur==false){
                        pivot.setUmur(pivotTemp.get(plist.getString(1)+"_A").getUmur());
                        setUmur=true;
                    }
                }
                if(pivotTemp.get(plist.getString(1)+"_S") != null){
                    pivot.setSepatu(pivotTemp.get(plist.getString(1)+"_S").getJumlah());
                    if(setUmur==false){
                        pivot.setUmur(pivotTemp.get(plist.getString(1)+"_S").getUmur());
                        setUmur=true;
                    }
                }                    
                    
                if(pivot.getUmur() != 0.0 || pivot.getAnak() != 0.0 || pivot.getSepatu() != 0.0 || pivot.getPria() != 0.0 || pivot.getWanita() != 0.0)
                    getPivotListJenis().add(pivot);
                setUmur=false;
            }                                                
        }
        
        saveData("dataPivot", new ArrayList<PivotJenis>(getPivotListJenis()));        //strip = null;pivot = null;str = null;ps = null;striplist = null;kd_strip=null;
        System.gc();
        return SUCCESS;
    }
    
    //Euclidean Distance
    public String euclideanDistance() throws SQLException, IOException, ClassNotFoundException{
        //JOptionPane.showMessageDialog(null, "EUCLID");
        getSession().remove("dataJarak");
        if(getThreshold() == 0.0)
            setThreshold((double)getSession().get("threshold"));
            
        factList = new ArrayList<Fact>((List<Fact>) getData("tabelFakta"));
        normalizedFact = new ArrayList<Fact>((List<Fact>) getData("dataNormal"));
        pivotListJenis = new ArrayList<PivotJenis>((List<PivotJenis>) getData("dataPivot"));
        jumlahJns = new HashMap<String, Integer>((HashMap<String, Integer>) getData("jumlahAsli"));
        
        //Make the very first cluster
        getSysCluster().add(new SystemCluster());
        //insert the first data into the first cluster
        getSysCluster().get(lastCluster-1).getCluster().add(pivotListJenis.get(0));
        //set attribute 'cluster' in pivotJenis
        pivotListJenis.get(0).setCluster(lastCluster);
        //add each kelompok jenis to the cluster, to get the total kelompok jenis in the final
        hitungJumlahJenis(0, lastCluster-1, pivotListJenis.get(0).getNo_nota());
        //last cluster become 2.
        lastCluster++;
        
        //Add New Distances which contains no nota, array of nodeDist and that node's cluster
        getDistances().add(new Distance(pivotListJenis.get(0).getNo_nota(), new ArrayList<NodeDist>(), pivotListJenis.get(0).getCluster()));
        //add new nodeDist to itself (0) into the nodeDist array above
        getDistances().get(0).getListJarak().add(new NodeDist(pivotListJenis.get(0).getNo_nota(), 0.00));
        for (int i = 1; i < pivotListJenis.size(); i++) {
            //Array of node dist for the new node's neigbors distances
            List <NodeDist> dists = new ArrayList<NodeDist>();
            //to save the cluster which new data got the closest neighbor            
            int clusterTemp = 1;
            //for saving the nearest distance
            double distTemp = 0.0;            
            
            for (int j = 0; j < i; j++) {
                //The main euclid below
                double dist = 0.0;
                //Calculate the dsitance from every stripes between two receipt
                dist += Math.pow(pivotListJenis.get(j).getUmur()-pivotListJenis.get(i).getUmur(),2);
                //System.out.println(dist+"=power("+pivotListJenis.get(j).getUmur()+"-"+pivotListJenis.get(i).getUmur()+",2)");
                dist += Math.pow(pivotListJenis.get(j).getPria()-pivotListJenis.get(i).getPria(),2);
                //System.out.println(dist+"=power("+pivotListJenis.get(j).getPria()+"-"+pivotListJenis.get(i).getPria()+",2)");
                //System.out.println();
                dist += Math.pow(pivotListJenis.get(j).getWanita()-pivotListJenis.get(i).getWanita(),2);
                dist += Math.pow(pivotListJenis.get(j).getAnak()-pivotListJenis.get(i).getAnak(),2);
                dist += Math.pow(pivotListJenis.get(j).getSepatu()-pivotListJenis.get(i).getSepatu(),2);
                
                dist = Math.sqrt(dist);
                //compare to get the nearest distance
                if(j==0)
                    distTemp=dist;
                else if(dist<distTemp){
                    distTemp=dist;
                    clusterTemp=pivotListJenis.get(j).getCluster();                    
                }
                //save distance from node j to i in a hash map
                dists.add(new NodeDist(pivotListJenis.get(j).getNo_nota(), dist));
                //save distance from node i to j on j's hash map (the above code's vice versa)
                getDistances().get(j).getListJarak().add(new NodeDist(pivotListJenis.get(i).getNo_nota(), dist));
                //distance.put(pivotList.get(j).getNo_nota()+"_"+pivotList.get(i).getNo_nota(),dist);                                
            }
            //save the node's distance to itself
            dists.add(new NodeDist(pivotListJenis.get(i).getNo_nota(), 0.00));
            
            if(distTemp < getThreshold()){
                //System.out.println("true:"+distTemp);
                pivotListJenis.get(i).setCluster(clusterTemp);
                getSysCluster().get(clusterTemp-1).getCluster().add(pivotListJenis.get(i));
                //sum the new node's jumlah of kelompok jenis to the cluster
                hitungJumlahJenis(i, clusterTemp-1, pivotListJenis.get(i).getNo_nota());
                /*if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_P")!=null)
                    getSysCluster().get(clusterTemp-1).setTotalPria(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_P"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_W")!=null)
                    getSysCluster().get(clusterTemp-1).setTotalWanita(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_W"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_A")!=null)
                    getSysCluster().get(clusterTemp-1).setTotalAnak(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_A"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_S")!=null)
                    getSysCluster().get(clusterTemp-1).setTotalSepatu(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_S"));*/
                
                //System.out.println("dist:"+distTemp+">="+getThreshold()+"a="+pivotListJenis.get(i).getAnak()+"s="+pivotListJenis.get(i).getSepatu()+"w="+pivotListJenis.get(i).getWanita()+"p="+pivotListJenis.get(i).getPria());
                //clstr[clusterTemp-1]+=1;               
            }else{
                getSysCluster().add(new SystemCluster());
                pivotListJenis.get(i).setCluster(lastCluster);                
                getSysCluster().get(lastCluster-1).getCluster().add(pivotListJenis.get(i));

                hitungJumlahJenis(i, lastCluster-1, pivotListJenis.get(i).getNo_nota());
                /*if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_P")!=null)
                    getSysCluster().get(lastCluster-1).setTotalPria(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_P"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_W")!=null)
                    getSysCluster().get(lastCluster-1).setTotalWanita(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_W"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_A")!=null)
                    getSysCluster().get(lastCluster-1).setTotalAnak(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_A"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_S")!=null)
                    getSysCluster().get(lastCluster-1).setTotalSepatu(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_S"));*/
                
                //clstr[lastCluster-1]+=1;                                
                //System.out.println("dist:"+distTemp+"<"+getThreshold());
                lastCluster++;                
            }
            //System.out.println("dist:"+distTemp+"c="+pivotListJenis.get(i).getCluster()+"a="+pivotListJenis.get(i).getAnak()+"s="+pivotListJenis.get(i).getSepatu()+"w="+pivotListJenis.get(i).getWanita()+"p="+pivotListJenis.get(i).getPria());
            getDistances().add(new Distance(pivotListJenis.get(i).getNo_nota(), dists, pivotListJenis.get(i).getCluster()));
            dists=null;clusterTemp=0;distTemp=0.0;
        }
        saveData("dataPivot", new ArrayList<PivotJenis>(getPivotListJenis()));
        getSession().put("dataJarak", new ArrayList<Distance>(getDistances()));
        getSession().put("systemCluster", new ArrayList<SystemCluster>(getSysCluster()));
        System.gc();
        return SUCCESS;
    }
    
    public void hitungJumlahJenis(int index_nota, int index_cluster, String nota){
        //You've set to += not = anymore. It's to get the total sum of each kelompok jenis
        if(jumlahJns.get(pivotListJenis.get(index_nota).getNo_nota()+"_P")!=null)
            getSysCluster().get(index_cluster).setTotalPria(jumlahJns.get(pivotListJenis.get(index_nota).getNo_nota()+"_P"));
        if(jumlahJns.get(pivotListJenis.get(index_nota).getNo_nota()+"_W")!=null)
            getSysCluster().get(index_cluster).setTotalWanita(jumlahJns.get(pivotListJenis.get(index_nota).getNo_nota()+"_W"));
        if(jumlahJns.get(pivotListJenis.get(index_nota).getNo_nota()+"_A")!=null)
            getSysCluster().get(index_cluster).setTotalAnak(jumlahJns.get(pivotListJenis.get(index_nota).getNo_nota()+"_A"));
        if(jumlahJns.get(pivotListJenis.get(index_nota).getNo_nota()+"_S")!=null)
            getSysCluster().get(index_cluster).setTotalSepatu(jumlahJns.get(pivotListJenis.get(index_nota).getNo_nota()+"_S"));                    
    }
        
    //Cosine Similarity Encapsulated ArrayList
    public String cosineSimilarityObject() throws SQLException, IOException, ClassNotFoundException{
        //JOptionPane.showMessageDialog(null, "COSINE OBJECT");
        getSession().remove("dataJarak");
        if(getThreshold() == 0.0)
            setThreshold((double)getSession().get("threshold"));
            
        factList = new ArrayList<Fact>((List<Fact>) getData("tabelFakta"));
        normalizedFact = new ArrayList<Fact>((List<Fact>) getData("dataNormal"));
        pivotListJenis = new ArrayList<PivotJenis>((List<PivotJenis>) getData("dataPivot"));
        jumlahJns = new HashMap<String, Integer>((HashMap<String, Integer>) getData("jumlahAsli"));
        
        getSysCluster().add(new SystemCluster());
        getSysCluster().get(lastCluster-1).getCluster().add(pivotListJenis.get(0));
        pivotListJenis.get(0).setCluster(lastCluster);
        hitungJumlahJenis(0, lastCluster-1, pivotListJenis.get(0).getNo_nota());
        lastCluster++;
        
        getDistances().add(new Distance(pivotListJenis.get(0).getNo_nota(), new ArrayList<NodeDist>(), pivotListJenis.get(0).getCluster()));
        getDistances().get(0).getListJarak().add(new NodeDist(pivotListJenis.get(0).getNo_nota(), 0.00));
        for (int i = 1; i < pivotListJenis.size(); i++) {
            List <NodeDist> dists = new ArrayList<NodeDist>();            
            int clusterTemp = 1;
            double distTemp = 0.0;            
            
            for (int j = 0; j < i; j++) {
                double dist = 0.0;
                //Calculate the dsitance from every stripes between two receipt
                double dot = pivotListJenis.get(i).getUmur()*pivotListJenis.get(j).getUmur()+pivotListJenis.get(i).getPria()*pivotListJenis.get(j).getPria()+pivotListJenis.get(i).getWanita()*pivotListJenis.get(j).getWanita()+pivotListJenis.get(i).getAnak()*pivotListJenis.get(j).getAnak()+pivotListJenis.get(i).getSepatu()*pivotListJenis.get(j).getSepatu();
                //System.out.println("DOT||"+dot+"="+pivotListJenis.get(i).getUmur()+"*"+pivotListJenis.get(j).getUmur()+"+"+pivotListJenis.get(i).getPria()+"*"+pivotListJenis.get(j).getPria()+"+"+pivotListJenis.get(i).getWanita()+"*"+pivotListJenis.get(j).getWanita()+"+"+pivotListJenis.get(i).getAnak()+"*"+pivotListJenis.get(j).getAnak()+"+"+pivotListJenis.get(i).getSepatu()+"*"+pivotListJenis.get(j).getSepatu());
                //Make two digits behind the dot
                double absoluted1 = Math.sqrt(pivotListJenis.get(i).getUmur()*pivotListJenis.get(i).getUmur()+pivotListJenis.get(i).getPria()*pivotListJenis.get(i).getPria()+pivotListJenis.get(i).getWanita()*pivotListJenis.get(i).getWanita()+pivotListJenis.get(i).getAnak()*pivotListJenis.get(i).getAnak()+pivotListJenis.get(i).getSepatu()*pivotListJenis.get(i).getSepatu());
                //System.out.println("ABSLT1||"+absoluted1+"=sqrt("+pivotListJenis.get(i).getUmur()+"*"+pivotListJenis.get(i).getUmur()+"+"+pivotListJenis.get(i).getPria()+"*"+pivotListJenis.get(i).getPria()+"+"+pivotListJenis.get(i).getWanita()+"*"+pivotListJenis.get(i).getWanita()+"+"+pivotListJenis.get(i).getAnak()+"*"+pivotListJenis.get(i).getAnak()+"+"+pivotListJenis.get(i).getSepatu()+"*"+pivotListJenis.get(i).getSepatu()+")");
                double absoluted2 = Math.sqrt(pivotListJenis.get(j).getUmur()*pivotListJenis.get(j).getUmur()+pivotListJenis.get(j).getPria()*pivotListJenis.get(j).getPria()+pivotListJenis.get(j).getWanita()*pivotListJenis.get(j).getWanita()+pivotListJenis.get(j).getAnak()*pivotListJenis.get(j).getAnak()+pivotListJenis.get(j).getSepatu()*pivotListJenis.get(j).getSepatu());
                //System.out.println("ABSLT2||"+absoluted2+"=sqrt("+pivotListJenis.get(j).getUmur()+"*"+pivotListJenis.get(j).getUmur()+"+"+pivotListJenis.get(j).getPria()+"*"+pivotListJenis.get(j).getPria()+"+"+pivotListJenis.get(j).getWanita()+"*"+pivotListJenis.get(j).getWanita()+"+"+pivotListJenis.get(j).getAnak()+"*"+pivotListJenis.get(j).getAnak()+"+"+pivotListJenis.get(j).getSepatu()+"*"+pivotListJenis.get(j).getSepatu()+")");
                //save distance from node j to i in a hash map
                dist = dot/(absoluted1*absoluted2);
                //System.out.println(dist+"="+dot+"/"+absoluted1+"*"+absoluted2);
                //if(Double.isNaN(dist))
                    //System.out.println(pivotListJenis.get(j).getUmur()+" "+pivotListJenis.get(j).getPria()+" "+pivotListJenis.get(j).getWanita()+" "+pivotListJenis.get(j).getAnak()+" "+pivotListJenis.get(j).getSepatu());
                if(j==0)
                    distTemp=dist;
                else if(dist>distTemp){
                    distTemp=dist;
                    clusterTemp=pivotListJenis.get(j).getCluster();                    
                }
                dists.add(new NodeDist(pivotListJenis.get(j).getNo_nota(), dist));
                //save distance from node i to j on j's hash map (the above code's vice versa)  
                getDistances().get(j).getListJarak().add(new NodeDist(pivotListJenis.get(i).getNo_nota(), dist));
                //distance.put(pivotList.get(j).getNo_nota()+"_"+pivotList.get(i).getNo_nota(),dist);                                
            }
            dists.add(new NodeDist(pivotListJenis.get(i).getNo_nota(), 0.00));
            
            if(distTemp > getThreshold()){
                //System.out.println("true:"+distTemp);
                pivotListJenis.get(i).setCluster(clusterTemp);
                getSysCluster().get(clusterTemp-1).getCluster().add(pivotListJenis.get(i));                
                
                hitungJumlahJenis(i, clusterTemp-1, pivotListJenis.get(i).getNo_nota());
                /*if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_P")!=null)
                    getSysCluster().get(clusterTemp-1).setTotalPria(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_P"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_W")!=null)
                    getSysCluster().get(clusterTemp-1).setTotalWanita(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_W"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_A")!=null)
                    getSysCluster().get(clusterTemp-1).setTotalAnak(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_A"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_S")!=null)
                    getSysCluster().get(clusterTemp-1).setTotalSepatu(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_S"));*/
                
                //System.out.println("dist:"+distTemp+">="+getThreshold()+"a="+pivotListJenis.get(i).getAnak()+"s="+pivotListJenis.get(i).getSepatu()+"w="+pivotListJenis.get(i).getWanita()+"p="+pivotListJenis.get(i).getPria());
                //clstr[clusterTemp-1]+=1;               
            }else{
                getSysCluster().add(new SystemCluster());
                pivotListJenis.get(i).setCluster(lastCluster);                
                getSysCluster().get(lastCluster-1).getCluster().add(pivotListJenis.get(i));                
                
                hitungJumlahJenis(i, lastCluster-1, pivotListJenis.get(i).getNo_nota());
                /*if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_P")!=null)
                    //You've changed set to +=
                    getSysCluster().get(lastCluster-1).setTotalPria(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_P"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_W")!=null)
                    getSysCluster().get(lastCluster-1).setTotalWanita(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_W"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_A")!=null)
                    getSysCluster().get(lastCluster-1).setTotalAnak(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_A"));
                if(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_S")!=null)
                    getSysCluster().get(lastCluster-1).setTotalSepatu(jumlahJns.get(pivotListJenis.get(i).getNo_nota()+"_S"));*/
                
                //clstr[lastCluster-1]+=1;                                
                //System.out.println("dist:"+distTemp+"<"+getThreshold());
                lastCluster++;                
            }
            
            getDistances().add(new Distance(pivotListJenis.get(i).getNo_nota(), dists, pivotListJenis.get(i).getCluster()));
            dists=null;clusterTemp=0;distTemp=0.0;
            //System.out.println("c="+pivotListJenis.get(i).getCluster()+"a="+pivotListJenis.get(i).getAnak()+"s="+pivotListJenis.get(i).getSepatu()+"w="+pivotListJenis.get(i).getWanita()+"p="+pivotListJenis.get(i).getPria());
        }
        
        saveData("dataPivot", new ArrayList<PivotJenis>(getPivotListJenis()));
        getSession().put("dataJarak", new ArrayList<Distance>(getDistances()));
        getSession().put("systemCluster", new ArrayList<SystemCluster>(getSysCluster()));
        System.gc();        
        return SUCCESS;
    }
    
    //Clustering data manually for purity check
    public String clusterManual() throws IOException, SQLException, ClassNotFoundException{
        //JOptionPane.showMessageDialog(null, "ClusterManual");
        factList = (List<Fact>) getData("tabelFakta");
        normalizedFact = (List<Fact>) getData("dataNormal");
        pivotListJenis = (List<PivotJenis>) getData("dataPivot");
        sysCluster = new ArrayList<SystemCluster>((List<SystemCluster>) getSession().get("systemCluster"));
        umurTemp = (HashMap<String, Integer>) getData("umurAsli");  
        jumlahJns = (HashMap<String, Integer>) getData("jumlahAsli");
        
        double minUmur = 0.0, maxUmur = 0.0;        
        double rentang = 0.0, temp = 0.0;
        int minUmurReal = 0, maxUmurReal = 0, rentangReal = 0, tempReal = 0;
        
        //Kalo sempet optimalisasi pake comparable
        for (int i = 0; i < pivotListJenis.size(); i++) {
            if(pivotListJenis.get(i).getUmur() > maxUmur){
                maxUmur = pivotListJenis.get(i).getUmur();
                maxUmurReal = umurTemp.get(pivotListJenis.get(i).getNo_nota());
            }else if(pivotListJenis.get(i).getUmur() < minUmur){
                minUmur = pivotListJenis.get(i).getUmur();
                minUmurReal = umurTemp.get(pivotListJenis.get(i).getNo_nota());
            }            
        }
        temp = minUmur;
        tempReal = minUmurReal;
        rentang = (maxUmur-minUmur)/3;
        rentangReal = (maxUmurReal-minUmurReal)/3;
        //System.out.println(temp+" R"+tempReal+" "+rentang+" R"+rentangReal);
        /*rentang = (maxUmur-minUmur)/4;
        rentangReal = (maxUmurReal-minUmurReal)/4;*/
        //<= ubah sesuai jumlah bagi diatas
        for (int i = 1; i <= 3; i++) {            
            manCluster.add(new ManualCluster("Golongan "+i+" P", tempReal, tempReal+rentangReal, "P"));
            manCluster.add(new ManualCluster("Golongan "+i+" W", tempReal, tempReal+rentangReal, "W"));
            manCluster.add(new ManualCluster("Golongan "+i+" A", tempReal, tempReal+rentangReal, "A"));
            manCluster.add(new ManualCluster("Golongan "+i+" S", tempReal, tempReal+rentangReal, "S"));
            tempReal += rentangReal;
        }
                
        for (int i = 0; i < pivotListJenis.size(); i++) {            
            manCluster.get(ClusterCheck(i,pivotListJenis.get(i).getNo_nota(),minUmurReal,maxUmurReal,rentangReal)).getCluster().add(pivotListJenis.get(i));          
        }
        
        int[] dominan;
        //int[] perDominan = new int[getSysCluster().size()];
        int totalDominan = 0;
        int totalData = 0;
        Set<String> hs = new HashSet<String>();
        Map<String, Integer> map = new HashMap<String, Integer>();
        //Iterate over all produced SystemCluster
        for (int i = 0; i < getSysCluster().size(); i++) {
            //Reinit array for every loop. 12 for 3 golongan, and 16 for 4 of em.   
            dominan = new int[12];
            //Iterate over each SystemCuster's data
            for (int j = 0; j < getSysCluster().get(i).getCluster().size(); j++) {
                //Check each data for which manualcluster current data belongs to, and then add dominan with index produced by one                                                             
                dominan[ClusterCheck(j,sysCluster.get(i).getCluster().get(j).getNo_nota(),minUmurReal,maxUmurReal,rentangReal)]+=1;                                                                                
            }
            //System.out.println(dominan[0]+" "+dominan[1]+" "+dominan[2]+" "+dominan[3]+" "+dominan[4]+" "+dominan[5]+" "+dominan[6]+" "+dominan[7]+" "+dominan[8]+" "+dominan[9]+" "+dominan[10]+" "+dominan[11]);
            List b = Arrays.asList(ArrayUtils.toObject(dominan));
            //get the largest number of the dominan
            getSysCluster().get(i).setDominan((int) Collections.max(b));
            //get the index of the largest element in dominan for naming the SystemCluster
            getSysCluster().get(i).setNama(manCluster.get(b.indexOf(Collections.max(b))).getNama());            
            //save count of dominant data for each cluster (for user display in purity)            
            //I guess from this point are all self explanainable
            if(hs.add(manCluster.get(b.indexOf(Collections.max(b))).getNama()))
                totalDominan += (int) Collections.max(b);
            else
                map.put(manCluster.get(b.indexOf(Collections.max(b))).getNama(), (int) Collections.max(b));
            totalData += getSysCluster().get(i).getCluster().size();
            dominan = null;
            b = null;            
        }
        //if there are two or more cluster with same dominant, select the one with largest dominant node
        for (int i = 0; i < sysCluster.size(); i++) {
            if(map.get(sysCluster.get(i).getNama())!=null){
                if(sysCluster.get(i).getDominan() < map.get(sysCluster.get(i).getNama())){
                    totalDominan-=sysCluster.get(i).getDominan();
                    totalDominan+=map.get(sysCluster.get(i).getNama());
                }                    
            }
            //System.out.println(sysCluster.get(i).getNama()+" ");
        }
        
        double purity = 0.0; hs=null;
        purity = ((double) totalDominan)/((double) totalData);       
        getSession().put("purity", purity);
        getSession().put("dominan", totalDominan);
        getSession().put("totalData", totalData);
        getSession().put("manualCluster", manCluster);
        getSession().put("systemCluster", new ArrayList<SystemCluster>(sysCluster));                
        
        System.gc();        
        return SUCCESS;
    }
            
    public int ClusterCheck(int i, String nota, double minUmur, double maxUmur, double rentang){
        int clusterIndex=0, cc=0, cd=0;
        int u=0, p=0, w=0, a=0, s=0;
        u = umurTemp.get(nota);
        if(jumlahJns.get(nota+"_P") != null)
            p = jumlahJns.get(nota+"_P");
        if(jumlahJns.get(nota+"_W") != null)
            w = jumlahJns.get(nota+"_W");
        if(jumlahJns.get(nota+"_A") != null)
            a = jumlahJns.get(nota+"_A");
        if(jumlahJns.get(nota+"_S") != null)
            s = jumlahJns.get(nota+"_S");
        /*if(("B-01-0212-00131").equals(p.getNo_nota()))
            System.out.println(p.getPria()+" "+p.getWanita()+" "+p.getAnak()+" "+p.get);*/
        if(u >= minUmur && u <= minUmur+rentang){
            clusterIndex=0;           
        }else if(u > minUmur+rentang && u <= minUmur+(rentang*2)){
            clusterIndex=4;           
        }else if(u > minUmur+(rentang*2) && u <= minUmur+(rentang*3)){
            clusterIndex=8;            
        }/*else if(p.getUmur() > minUmur+(rentang*3) && p.getUmur() <= maxUmur){
            clusterIndex=12;            
        }*/
        
        //System.out.println(cc+" "+minUmur+" "+rentang+" pu:"+p.getUmur());
        if(p != 0 &&
          ((p > w) &&
          (p > a) &&
          (p > s))){
            //System.out.println("_"+p.getNo_nota()+"_ masuk pria 0");            
            clusterIndex+=0;            
        }else if(w != 0 &&
          ((w > p) &&
          (w > a) &&
          (w > s))){           
            clusterIndex+=1;            
        }else if(a != 0 &&
          ((a > p) &&
          (a > w) &&
          (a > s))){
            clusterIndex+=2;            
        }else if(s != 0 &&
          ((s > p) &&
          (s > w) &&
          (s > a))){
            clusterIndex+=3;            
        }       
        
        //System.out.println("cd:"+cd+" p:"+p.getPria()+" w:"+p.getWanita()+" a:"+p.getAnak()+" s:"+p.getSepatu());
        return clusterIndex;
    }
    
    public String scatterPlot() throws SQLException, IOException, ClassNotFoundException{
        //JOptionPane.showMessageDialog(null, "SCATTER PLOT");
        factList = (List<Fact>) getData("tabelFakta");
        normalizedFact = (List<Fact>) getData("dataNormal");
        pivotListJenis = (List<PivotJenis>) getData("dataPivot");
        sysCluster = new ArrayList<SystemCluster>((List<SystemCluster>) getSession().get("systemCluster"));                        
        
        Double x=0.0;
        Double y=0.0;                        
        for (int i = 0; i < pivotListJenis.size(); i++) {
            double wAA=0,wAB=0,wAC=0,wAD=0,wAE=0;
            double wBA=0,wBB=0,wBC=0,wBD=0;
            double a=pivotListJenis.get(i).getUmur();
            double b=pivotListJenis.get(i).getPria();
            double c=pivotListJenis.get(i).getWanita();
            double d=pivotListJenis.get(i).getAnak();
            double e=pivotListJenis.get(i).getSepatu();
            
            wAA=(1/Math.sqrt(5))*a;
            wAB=(1/Math.sqrt(5))*b;
            wAC=(1/Math.sqrt(5))*c;
            wAD=(1/Math.sqrt(5))*d;
            wAE=(1/Math.sqrt(5))*e;            
                        
            wBA=(-1/Math.sqrt(5))*a;
            wBB=(1/Math.sqrt(5))*b;
            wBC=(-1/Math.sqrt(5))*c;
            wBD=(1/Math.sqrt(5))*d;
                        
            x=wAA+wAB+wAC+wAD+wAE;
            y=wBA+wBB+wBC+wBD;           
            
            //System.out.println(wAA+" "+wAB+" "+wAC+" "+wAD+" "+wAE);
            //System.out.println(wBA+" "+wBB+" "+wBC+" "+wBD);
            //System.out.println("x:"+x+" y:"+y+" c:"+pivotListJenis.get(i).getCluster());            
            //System.out.println("");
            
            pivotListJenis.get(i).setX(x);
            pivotListJenis.get(i).setY(y);                                                
        }        
        
        return Action.SUCCESS;       
    }                        
    
    public String displayFactList() throws ClassNotFoundException, SQLException
    {   
        String str = (String) getSession().get("USER");
        getSession().clear();
        getSession().put("USER", str);
        str = null;
        clearData();                
        
        try
        {
            //make connection            
            connection = hc.getConn();
            //str= "select * from `bon_fact_jenis` where tanggal between '"+getStartDate()+"' AND '"+getEndDate()+"'"; 
            str= "select * from `tunai_fact_jenis` where tanggal between '"+getStartDate()+"' AND '"+getEndDate()+"'";             
            Fact fact;
            Fact norm;

            ps = connection.prepareStatement(str);
            flist =ps.executeQuery();
            while(flist.next())
            {
                fact=new Fact();
                norm=new Fact();
                fact.setTanggal(flist.getDate(1));
                fact.setNo_nota(flist.getString(2));                
                fact.setKd_customer(flist.getString(3));
                fact.setUmur(flist.getDouble(4));
                fact.setKel_jns(flist.getString(5));
                fact.setJumlah(flist.getDouble(6));
                fact.setId_toko(flist.getInt(7));
                norm.setTanggal(flist.getDate(1));
                norm.setNo_nota(flist.getString(2));                
                norm.setKd_customer(flist.getString(3));
                norm.setUmur(flist.getDouble(4));
                norm.setKel_jns(flist.getString(5));
                norm.setJumlah(flist.getDouble(6));
                norm.setId_toko(flist.getInt(7));                                                                               
                factList.add(fact);
                Facts.add(norm);
                jumlahJns.put(flist.getString(2)+"_"+flist.getString(5), flist.getInt(6));
                umurTemp.put(flist.getString(2), flist.getInt(4));
                //System.out.println("_"+flist.getString(2)+"_"+flist.getString(3)+"_"+flist.getString(5)+"_"+flist.getDouble(6)+"_");
            }
            saveData("jumlahAsli", jumlahJns);
            saveData("umurAsli", umurTemp);
            saveData("tabelFakta", new ArrayList<Fact>(factList));
            saveData("Normalisasi", new ArrayList<Fact>(Facts));
            saveData("startDate", startDate);
            saveData("endDate", endDate);            
            str=null;ps=null;fact=null;norm=null;flist=null;                                   
            System.gc();
                
        } catch(Exception e)
        {
            e.printStackTrace();
            return ERROR;
        }
        
        return SUCCESS;
    }
    
    public Object getData(String key) throws SQLException, IOException, ClassNotFoundException {
        ObjectInputStream ois = null;
        Connection conn = mc.getConn();
        Statement stmt = conn.createStatement();        
        ResultSet rs = stmt.executeQuery("SELECT `value` FROM `session` WHERE `data_id` = '"+key+"'");
        if (rs.isBeforeFirst()) {
            rs.next();            
            byte[] st = (byte[]) rs.getObject(1);
            ByteArrayInputStream bais = new ByteArrayInputStream(st);
            ois = new ObjectInputStream(bais);
            return (Object) ois.readObject();
        }
        
        stmt.close();
        rs.close();
        //  conn.close();        
        
        return null;
    }
    
    public String saveData(String key, Object ob) throws IOException, SQLException {
        Connection conn = mc.getConn();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(ob);
        byte[] objectAsBytes = baos.toByteArray();
        PreparedStatement pstmt = conn.prepareStatement("REPLACE INTO `session` (`data_id`, `value`) VALUES(?, ?)");
        ByteArrayInputStream bais = new ByteArrayInputStream(objectAsBytes);
        pstmt.setString(1, key);
        pstmt.setBinaryStream(2, bais, objectAsBytes.length);
        pstmt.executeUpdate();
        pstmt.close();
        //conn.close();
                        
        return SUCCESS;
    }    
    
    public String clearData() throws SQLException{
        Connection conn = mc.getConn();
        PreparedStatement ps = conn.prepareStatement("DELETE FROM `session`");
        ps.executeUpdate();
        ps.close();
        //conn.close();
        
        return SUCCESS;
    }    

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(String startDate){
        try {
            this.startDate = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyy/MM/dd").parse(startDate));
        } catch (ParseException ex) {
            Logger.getLogger(FetchData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate){
        try {
            this.endDate = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyy/MM/dd").parse(endDate));
        } catch (ParseException ex) {
            Logger.getLogger(FetchData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the factList
     */
    public List<Fact> getFactList() {
        return factList;
    }

    /**
     * @param factList the factList to set
     */
    public void setFactList(List<Fact> factList) {
        this.factList = factList;
    }

    /**
     * @return the normalizedFact
     */
    public List<Fact> getNormalizedFact() {
        return normalizedFact;
    }

    /**
     * @param normalizedFact the normalizedFact to set
     */
    public void setNormalizedFact(List<Fact> normalizedFact) {
        this.normalizedFact = normalizedFact;
    }

    /**
     * @return the pivotList
     */
    public List<Pivot> getPivotList() {
        return pivotList;
    }

    /**
     * @param pivotList the pivotList to set
     */
    public void setPivotList(List<Pivot> pivotList) {
        this.pivotList = pivotList;
    }

    /**
     * @return the distances
     */
    public List<Distance> getDistances() {
        return distances;
    }

    /**
     * @param distances the distances to set
     */
    public void setDistances(List<Distance> distances) {
        this.distances = distances;
    }

    /**
     * @return the threshold
     */
    public double getThreshold() {
        return threshold;
    }

    /**
     * @param threshold the threshold to set
     */
    public void setThreshold(double threshold) {
        getSession().remove("threshold");
        this.threshold =  (double) threshold;
        getSession().put("threshold", (double) threshold);
    }

    /**
     * @return the pivotListJenis
     */
    public List<PivotJenis> getPivotListJenis() {
        return pivotListJenis;
    }

    /**
     * @param pivotListJenis the pivotListJenis to set
     */
    public void setPivotListJenis(List<PivotJenis> pivotListJenis) {
        this.pivotListJenis = pivotListJenis;
    }

    /**
     * @return the manCluster
     */
    public List<ManualCluster> getManCluster() {
        return manCluster;
    }

    /**
     * @param manCluster the manCluster to set
     */
    public void setManCluster(List<ManualCluster> manCluster) {
        this.manCluster = manCluster;
    }

    /**
     * @return the sysCluster
     */
    public List<SystemCluster> getSysCluster() {
        return sysCluster;
    }

    /**
     * @param sysCluster the sysCluster to set
     */
    public void setSysCluster(List<SystemCluster> sysCluster) {
        this.sysCluster = sysCluster;
    }

    /**
     * @return the session
     */
    public Map<String, Object> getSession() {
        return session;
    }

    /**
     * @param session the session to set
     */
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    /**
     * @return the pjTemp
     */
    public PivotJenis getPjTemp() {
        return pjTemp;
    }

    /**
     * @param pjTemp the pjTemp to set
     */
    public void setPjTemp(PivotJenis pjTemp) {
        this.pjTemp = pjTemp;
    }
}