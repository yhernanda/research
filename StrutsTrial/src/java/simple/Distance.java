/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author yhernanda
 */
public class Distance implements Serializable {
    public Distance(){
        
    }
    
    public Distance(String no_nota, List<NodeDist> jarak){
        this.no_nota = no_nota;
        this.listJarak = jarak;
    }
    
    public Distance(String no_nota, List<NodeDist> jarak, int cluster){
        this.no_nota = no_nota;
        this.listJarak = jarak;
        this.cluster = cluster;
    }
    
    private String no_nota;
    private List <NodeDist> listJarak = new ArrayList<NodeDist>();
    private int cluster;

    /**
     * @return the no_nota
     */
    public String getNo_nota() {
        return no_nota;
    }

    /**
     * @param no_nota the no_nota to set
     */
    public void setNo_nota(String no_nota) {
        this.no_nota = no_nota;
    }

    /**
     * @return the cluster
     */
    public int getCluster() {
        return cluster;
    }

    /**
     * @param cluster the cluster to set
     */
    public void setCluster(int cluster) {
        this.cluster = cluster;
    }

    /**
     * @return the listJarak
     */
    public List <NodeDist> getListJarak() {
        return listJarak;
    }

    /**
     * @param listJarak the listJarak to set
     */
    public void setListJarak(List <NodeDist> listJarak) {
        this.listJarak = listJarak;
    }
}