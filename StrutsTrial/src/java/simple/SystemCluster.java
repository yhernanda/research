/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yhernanda
 */
public class SystemCluster implements Serializable {
    private String nama;
    private String anggota;
    private int dominan;
    private int totalPria;
    private int totalWanita;
    private int totalAnak;
    private int totalSepatu;
    private List<PivotJenis> cluster;
    
    public SystemCluster(){
        this.nama = "";
        this.dominan = 0;
        this.totalPria = 0;
        this.totalWanita = 0;
        this.totalAnak = 0;
        this.totalSepatu = 0;
        this.cluster = new ArrayList<PivotJenis>();
    }
    
    public SystemCluster(String nama, int dominan){
        this.nama = nama;
        this.totalPria = 0;
        this.totalWanita = 0;
        this.totalAnak = 0;
        this.totalSepatu = 0;
        this.dominan = dominan;        
        this.cluster = new ArrayList<PivotJenis>();
    }
    
    public String getAnggota(){
        anggota = "";
        for (int i = 0; i < cluster.size(); i++) {
            anggota += cluster.get(i).getNo_nota();
            if(i != cluster.size()-1)
                anggota += ", ";
        }
        return anggota;
    }

    /**
     * @return the nama
     */
    public String getNama() {
        return nama;
    }

    /**
     * @param nama the nama to set
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     * @return the cluster
     */
    public List<PivotJenis> getCluster() {
        return cluster;
    }

    /**
     * @param cluster the cluster to set
     */
    public void setCluster(List<PivotJenis> cluster) {
        this.cluster = cluster;
    }

    /**
     * @return the dominan
     */
    public int getDominan() {
        return dominan;
    }

    /**
     * @param dominan the dominan to set
     */
    public void setDominan(int dominan) {
        this.dominan = dominan;
    }

    /**
     * @return the totalPria
     */
    public int getTotalPria() {
        return totalPria;
    }

    /**
     * @param totalPria the totalPria to set
     */
    public void setTotalPria(int totalPria) {
        this.totalPria += totalPria;
    }

    /**
     * @return the totalWanita
     */
    public int getTotalWanita() {
        return totalWanita;
    }

    /**
     * @param totalWanita the totalWanita to set
     */
    public void setTotalWanita(int totalWanita) {
        this.totalWanita += totalWanita;
    }

    /**
     * @return the totalAnak
     */
    public int getTotalAnak() {
        return totalAnak;
    }

    /**
     * @param totalAnak the totalAnak to set
     */
    public void setTotalAnak(int totalAnak) {
        this.totalAnak += totalAnak;
    }

    /**
     * @return the totalSepatu
     */
    public int getTotalSepatu() {
        return totalSepatu;
    }

    /**
     * @param totalSepatu the totalSepatu to set
     */
    public void setTotalSepatu(int totalSepatu) {
        this.totalSepatu += totalSepatu;
    }
}
