/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author yhernanda
 */
public class MySQLConnector {
    private Connection conn;
    private String driverName = "org.mariadb.jdbc.Driver";
    private String host = "jdbc:mariadb://localhost:3306/amigoData";
    private String user = "root";
    private String pass = "mariadbroot";    
    
    public MySQLConnector(){
        try{
            Class.forName(this.driverName);
            this.conn = DriverManager.getConnection(host, user, pass);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    
    public Connection getConn(){
        return conn;
    }
}
