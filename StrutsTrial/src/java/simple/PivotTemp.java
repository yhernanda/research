/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import java.io.Serializable;

/**
 *
 * @author yhernanda
 */
public class PivotTemp implements Serializable{   
    private Double jumlah;
    private Double umur;
    
    public PivotTemp(){
        
    }
    
    public PivotTemp(double jumlah, double umur){        
        this.jumlah = jumlah;
        this.umur = umur;
    }

    /**
     * @return the jumlah
     */
    public Double getJumlah() {
        return jumlah;
    }

    /**
     * @param jumlah the jumlah to set
     */
    public void setJumlah(Double jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public String toString() {
        return "PivotTemp{jumlah=" + jumlah + "umur" + umur + "}";
    }

    /**
     * @return the umur
     */
    public Double getUmur() {
        return umur;
    }

    /**
     * @param umur the umur to set
     */
    public void setUmur(Double umur) {
        this.umur = umur;
    }
    
    
}
